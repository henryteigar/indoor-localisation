The repository created for the Bachelor thesis on the following topic: Indoor Localisation Using the Received Signal Strength.

The main code is in the following two files:

* SPHERE_data_combiner.ipynb
	* should be run before using the method_implementations.ipynb notebook
	* converts the SPHERE training data into a correct format

* method_implementations.ipynb
	* Data pre-processing
	* k-NN, SVM, HMM implementations
	* Most of the visualisations
	* Implementation of the nested cross-validation
    
* initial_data_visualisations.R
	* This file is included as an extra. The code from this file was created to get a better understanding of the data and is directly not necessary for the implementaion processes.

* SPHERE_data_combiner.py and method_implementations.py
	* These are the exact same files as corresponding .ipynb files, only converted to .py format
