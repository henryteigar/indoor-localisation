
# coding: utf-8

# In[11]:


#This notebook is used to convert the raw SPHERE training data into 
#the format used for training and testing the models in the other notebook.


# In[ ]:


import sklearn
import pandas as pd
import numpy as np
import os


# In[ ]:


SPHERE_RECORDINGS_LOC = ""   # Location of the SPHERE training data
OUTPUT_DIRECTORY_LOC = ""    # Output directory of new data

# Creates an output direcory if not already exists
if not os.path.exists(OUTPUT_DIRECTORY_LOC):
    os.makedirs(OUTPUT_DIRECTORY_LOC)


# In[ ]:


sensor_datas = []
annotations = []

existing_dirs = os.listdir(SPHERE_RECORDINGS_LOC)
existing_dirs.sort()

# Reads in all the files in the specified location and adds the data into specific lists
for f_name in existing_dirs:
    if (f_name != '.DS_Store'):
        sensor_data = pd.read_csv(SPHERE_RECORDINGS_LOC + "/" + f_name + "/acceleration.csv")
        annotation = pd.read_csv(SPHERE_RECORDINGS_LOC + "/" + f_name + "/location_0.csv")
        sensor_datas.append(sensor_data)
        annotations.append(annotation)


# In[ ]:


# Structures the recordings based on the APs used in them
sensor_datas_00001_00005 = sensor_datas[0:5]
sensor_datas_00007_00010 = sensor_datas[6:]
sensor_fields_00001_00005 = ['t', 'Kitchen_AP', 'Lounge_AP', 'Upstairs_AP']
sensor_fields_00007_00010 = ['t', 'Kitchen_AP', 'Study_AP', 'Upstairs_AP']


# In[ ]:


# Helper method to get the correct room label that matches the specific time
def getRoom(data, time):
    room = data.query('start < ' + str(time) + " and end > " + str(time))
    count = room['name'].count()
    if count > 0: 
        return room.iloc[-1, 2]
    else:
        return np.nan


# In[ ]:


# This method is responsible for the "structural change" described in the thesis. It combines
# the information from two files into one
def combine_data(datas, fields, starting_index):
    for i, sensor_data in enumerate(datas):
        df = pd.DataFrame(sensor_data[fields])
        df['location'] = df.apply(lambda row: getRoom(annotations[i + starting_index], row['t']), axis=1)
        data_with_locations = df[pd.notnull(df['location'])]
        data_with_locations.to_csv(OUTPUT_DIRECTORY_LOC + "/combined_" + str(i+1+starting_index).zfill(2) + ".csv")


# In[ ]:


def start_combining():
    combine_data(sensor_datas_00001_00005, sensor_fields_00001_00005, 0)
    combine_data(sensor_datas_00007_00010, sensor_fields_00007_00010, 6)


# In[ ]:


start_combining()

