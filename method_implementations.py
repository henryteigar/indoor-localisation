
# coding: utf-8

# In[77]:


# All the below code is written by the author of the thesis, if not specified otherwise in the cell

# The code is written in a way to make it possible to use with different sensor setups and rooms
# It should be noted however, that it was mainly tested with recordings 1-5

# Before using this notebook, a converter should be run (SPHERE_data_combiner.ipynb) to convert the files
# into a structure used in this notebook


# In[2]:


import sklearn
import pandas as pd
import numpy as np
import random
from math import floor
import os
from sklearn.model_selection import train_test_split
import itertools
import matplotlib.pyplot as plt
from sklearn import svm, datasets
from sklearn.metrics import confusion_matrix
from sklearn import neighbors, svm, linear_model
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from hmmlearn import hmm
from multiprocessing import Pool
import time
import pickle
from collections import defaultdict


# In[3]:


# General conf:

COMBINED_LOC = "/Users/ml/Desktop/sphere_output" # Location of the output file converted with the other notebook
ROOMS = ['bath', 'bed2', 'living', 'kitchen', 'stairs', 'toilet', 'bed1', 'hall']
SENSORS_LOUNGE = ['Kitchen_AP', 'Lounge_AP', 'Upstairs_AP'] # This sensor setup is used in the thesis
SENSORS_STUDY = ['Kitchen_AP', 'Study_AP', 'Upstairs_AP']


# In[4]:


############################
## GENERAL HELPER METHODS ##
############################


# In[5]:


# Method for getting the exact sensor names based on the data

def get_sensor_names(data):
    columns = data.columns.values
    sensor_names = ['Kitchen_AP', 'Upstairs_AP']
    if 'Lounge_AP' in columns:
        sensor_names.insert(1, 'Lounge_AP')
    else:
        sensor_names.insert(1, 'Study_AP')
    return sensor_names


# In[6]:


# Method that loads the recordings converted with the converter. The input argument is the recrding nrs

def load_recordings_by_nrs(recording_nrs):
    existing_dirs = os.listdir(combined_loc)
    data_files = []

    for f_name in existing_dirs:
        file_nr = f_name.split("_")[1][:2]
        if file_nr in recording_nrs:
            data = pd.read_csv(combined_loc + "/" + f_name)
            data = data[['t'] + get_sensor_names(data) + ['location']]
            data_files.append(data)
            print("File", combined_loc + "/" + f_name, "loaded")
    return data_files


# In[7]:


# This method converts the room_ids into the room names
def get_state_names(state_nrs):
    names = []
    for state_nr in state_nrs:
        names.append(ROOMS[state_nr])
    return names


# In[83]:


# This method check for any controversial values in the configuration before the trainin process begins
def has_errors(train_data, test_data, train_usage, test_usage, shuffle, method, general_conf):
    if (general_conf['avg'] is not None and general_conf['avg'] > 1 and general_conf['nan-fill'] is None):
        print("Error! If avg is >1 then nan_fill must be specified")
        return True
    if (method == 'viterbi-means' and general_conf['nan-fill'] is None):
        print("Error! If using viterbi_means, nan_fill must be specified")
        return True
    if (method=="knn" or method=="svm") and general_conf["nan-fill"] == None:
        print("Error! With knn and svm, the empty values should be replaced before the training/testing process")
        return True
    return False


# In[9]:


###########################
## VISUALISATION METHODS ##
###########################


# In[78]:


def plot_confusion_matrix(test_y, pred_y, title):
    cnf_matrix = confusion_matrix(test_y, pred_y, labels=ROOMS)
    
    plt.imshow(cnf_matrix, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(ROOMS))
    plt.xticks(tick_marks, ROOMS, rotation=45)
    plt.yticks(tick_marks, ROOMS)
    thresh = cnf_matrix.max() / 2.
    for i, j in itertools.product(range(cnf_matrix.shape[0]), range(cnf_matrix.shape[1])):
        plt.text(j, i, format(cnf_matrix[i, j], 'd'),
                 horizontalalignment="center",
                 color="white" if cnf_matrix[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()


# In[11]:


def get_class_labels(splits, min_v, max_v):
    labels = []
    if splits == 1:
        labels.append("-inf -> inf")
    elif splits == 2:
        center = min_v + (max_v-min_v / 2)
        labels.append("empty values")
        labels.append("[" + str(round(center), 2) + "," + "0)")
    else:
        labels.append("empty values")
        step = (max_v-min_v) / (splits-2)
        prev_point = min_v
        for i in range(splits-2):
            labels.append("[" + str(round(prev_point, 2)) + "," + str(round(prev_point + step, 2)) + ")")
            prev_point += step
        labels.append("[" + str(round(max_v, 2)) + ",0)")
    return labels


# In[12]:


def visualize_emissions(emissions, sensor_names, conf):
    splits = conf["multinomial-splits"]
    min_value = conf["multinomial-min_value"]
    max_value = conf["multinomial-max_value"]
    
    for room in ROOMS:
        fig, ax = plt.subplots()
        fig.set_size_inches(15, 5)
        ax.set_title('Sensor values in ' + room)
        plt.tight_layout()
        for i, sensor in enumerate(sensor_names):
            ax.bar(range(i*splits + i,splits + i*splits + i), emissions[room+"_"+sensor], 0.9, align='center', label=sensor)
        ax.legend(prop={"size":17})
        ax.set_ylabel("Probability")
        ax.set_xlabel("Splits")
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
             ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(16)
        plt.xticks(range(3*splits + 2), (get_class_labels(splits, min_value, max_value) + [''])*3, rotation=90)
        fig.savefig("pildid/splits_" + room + ".eps", format="eps", bbox_inches='tight')
    plt.close()


# In[13]:


def visualize_predictions(test_y, pred_y):
    fig, ax = plt.subplots(figsize=(12,5))
    
    ax.plot(test_y, linewidth=4.0)
    ax.plot(pred_y, linewidth=1.3)
    ax.legend(['Correct','Predicted'], loc='upper left')
    fig.savefig("pildid/path.png", format="png", bbox_inches='tight', dpi=300)


# In[14]:


def plot_room_level_acc(test_y, pred_y):
    total_room_count = defaultdict(int)
    correct_room_count = defaultdict(int)
    
    for i, test in enumerate(test_y):
        total_room_count[test] += 1
        if (test == pred_y[i]):
            correct_room_count[test] += 1

    fig, ax = plt.subplots(figsize=(4, 4))
    
    room_accs = []
    for room in ROOMS:
        if (total_room_count[room] == 0):
            print("MISSING_VALUES")
            continue

        room_accs.append(correct_room_count[room] / total_room_count[room])
        
    ax.bar(np.arange(len(room_accs)), room_accs, 0.8)
    ax.set_xticks(np.arange(len(room_accs)))
    ax.set_xticklabels(ROOMS)
    plt.xticks(rotation=-45)    
    plt.savefig("pildid/room.png", format="png", bbox_inches='tight', dpi=300)


# In[91]:


def visualise_RSSI_data(data):
    fig, ax = plt.subplots(figsize=(16, 6))
    colors = ["#03a46d", "#e83505", "blue"]
    
    for i, sensor in enumerate(get_sensor_names(data)):
        ax.plot(data[sensor], linewidth=0.3, color=colors[i])


# In[15]:


###########################
## DATA PRE-PROCESSING ####
###########################


# In[16]:


# The main method for pre-processing the k-nearest neighbor.
# Parameter avg corresponds to the same parameter in the thesis
# Before using this method, the empty values should have been filled in

def get_data_averaged_over_last_n_frames(data, avg):
    data = data.copy()
    sensors = get_sensor_names(data)
    for sensor in sensors:
        if (data[sensor].isnull().values.any()):
            print("Nan values not allowed")
            return
        data[sensor] = data[sensor].rolling(min_periods=1, window=avg).mean()
    return data


# In[17]:


# Method to fill empty sensor values found in the data
def fill_nan_values(data, nan_fill):
    data = data.copy()
    sensors = get_sensor_names(data)
    for sensor in sensors:
        data[sensor] = data[sensor].fillna(nan_fill)
    return data


# In[18]:


###############################
## GENERAL DATA PROCESSING ####
###############################


# In[19]:


# Method used for splitting the data by specified train_usage and test_usage parameters
def split_data(data, train_usage, test_usage, shuffle=True):
    # Didn't use built in train_test_split as I wanted a bigger freedom of manipulating the system
    data = data.copy()

    if (shuffle):
        data = data.sample(frac=1)
    
    el_count = len(data)
    return data[:floor(el_count * train_usage)], data[floor(el_count * (1-test_usage)):]


# In[20]:


# Splits train and test data into train_X, train_y, test_X, test_y
def split_X_y(train, test):
    field_names = get_sensor_names(train)
    return train[field_names], train['location'], test[field_names], test['location'] # train_X, train_y, test_X, test_y


# In[21]:


# This method converts train_data and test_data into desired format and additional manipulation of the data
# is performed dependent on the arguments used.
def get_train_and_test_datasplits(train_data, test_data, train_usage, test_usage, shuffle, general_conf):
    train = train_data.copy()
    test = test_data.copy()
    
    nan_fill = general_conf['nan-fill']
    avg = general_conf['avg']
    
    if (nan_fill is not None):
        train = fill_nan_values(train, nan_fill)
        test = fill_nan_values(test, nan_fill)
        
    if (avg is not None and avg > 1):
        train = get_data_averaged_over_last_n_frames(train, avg)
        test = get_data_averaged_over_last_n_frames(test, avg)
    
    if (train is test):
        train_split, test_split = split_data(train, train_usage, test_usage, shuffle=shuffle)
    else:
        train_split, _ = split_data(train, train_usage, test_usage, shuffle=shuffle)
        _, test_split = split_data(test, train_usage, test_usage, shuffle=shuffle)
        
    return train_split, test_split


# In[22]:


###################################################
## NON_HARDCODED_TRANSITION_MATRIX FOR THE HMM ####
###################################################

# Note that this method also takes in "omega" parameter. The omega parameter with 
# non-hardcoded transition matrix was not covered in the thesis but was tested anyway and the 
# effect of the omega was similar when used with the hardcoded transition matrix
# If omega=0 then it does not have any effect.

def get_non_hardcoded_transition_matrix(labels, omega=0):
    count_matrix = np.zeros((len(ROOMS),len(ROOMS)))

    # First we create a transition count matrix which gets later converted into probability matrix
    for i, label in enumerate(labels):
        if (i == len(labels) - 1):
            break
        this_label = label
        next_label = labels[i+1]
        count_matrix[ROOMS.index(this_label)][ROOMS.index(next_label)] += 1
    
    # Conversion to the probability matrix
    trans_matrix = []
    for row in count_matrix:
        total_count = sum(row)
        new_row = []
        for count in row:
            new_row.append(count/total_count)
        trans_matrix.append(new_row)

    # Responsible for reducing the probability to switch to a separate room
    for i, row in enumerate(trans_matrix):
        prob_to_increase_same_room_prob = 0
        for j, count in enumerate(row):
            if (i!=j):
                old_value = trans_matrix[i][j]
                trans_matrix[i][j] /= 10**omega
                prob_to_increase_same_room_prob += old_value - trans_matrix[i][j]
        trans_matrix[i][i] += prob_to_increase_same_room_prob
    
    return trans_matrix


# In[23]:


###############################################
## HARDCODED_TRANSITION_MATRIX FOR THE HMM ####
###############################################

# Creates a hardcoded transition matrix.
# Takes the initial staying probability as an argument. Also an omega. 
# If omega=0 then it does not have any effect.

def get_hardcoded_transition_matrix(initial_stay_prob, omega=0):
    initial_switch_prob = 1 - initial_stay_prob
    switch = initial_switch_prob / 10**omega
    
    # This specific matrix is created by taking into account the actual SPHERE testing house floorplan
    stay = 1-switch 
    return np.array([[stay, 0.0, 0.0, 0.0, switch, 0.0, 0.0, 0.0],  # Bath
                     [0.0, stay, 0.0, 0.0, switch, 0.0, 0.0, 0.0],  # Bed2
                     [0.0, 0.0, stay, 0.0, switch/2, 0.0, 0.0, switch/2],  # Living
                     [0.0, 0.0, 0.0, stay, switch/2, 0.0, 0.0, switch/2],  # Kitchen
                     [switch/6, switch/6, switch/6, switch/6, stay, switch/6, switch/6, 0.0],  # Stairs
                     [0.0, 0.0, 0.0, 0.0, switch, stay, 0.0, 0.0],  # Toilet
                     [0.0, 0.0, 0.0, 0.0, switch, 0.0, stay, 0.0],  # Bed1
                     [0.0, 0.0, switch/2, switch/2, 0.0, 0.0, 0.0, stay],  # Hall                       
                   ])


# In[24]:


###################################
## EMISSION MATRIX FOR THE HMM ####
###################################

def get_emission_matrix(emissions, sensor_names):
    emission_matrix = []
    
    for room in ROOMS:
        room_sensors_emissions = []
        for sensor in sensor_names:
            emission = emissions[str(room) + "_" + str(sensor)]
            room_sensors_emissions.append(emission)
        
        room_emissions = []
        
        for i in room_sensors_emissions[0]:
            for j in room_sensors_emissions[1]:
                for k in room_sensors_emissions[2]:
                    room_emissions.append(i*j*k)
        emission_matrix.append(room_emissions)
    return emission_matrix


def get_emissions(data_X, data_y, room, sensor, classification_min=-110, classification_max=-60, splits=7):
    sensor_values = data_X[data_y == room][sensor]
    
    alpha = 1
    
    room_emissions_counts = np.array([alpha] * splits)
    total_count = np.array([len(sensor_values)] * splits)
    
    for value in sensor_values:
        class_id = get_split_id(value, splits, classification_min, classification_max)
        room_emissions_counts[class_id] += 1

    emissions = room_emissions_counts / (total_count + splits * alpha)
    
    if (str(sum(emissions)) != "1.0"):
        print("Error! Emission sum was not 1.0")
    return emissions


# In[38]:


################################
## HMM OTHER HELPER METHODS ####
################################


# In[ ]:


# Helper method used for the HMM approach that uses Gaussian distributions for the emissions.
# Was later left out of the thesis
def get_means_and_std(train_X, train_y):
    means = []
    stds = []
    for room in ROOMS:
        data = train_X[train_y == room]
        if (len(data) > 0):
            room_means = data.mean()
            std = data.std()
        else:
            print("Some data missing when calculating means")
            room_means = [-110, -110, -110]
            std = [0,0,0]
        means.append(room_means)
        stds.append(std)
    return means, stds


# In[ ]:


# A helper method for the HMM to convert a RSSI value into a split_id. 
# This information can later be used to create the emission matrix
# splits, min_value and max_value params must be the same that were used to create the   

def get_split_id(value, splits, min_value, max_value):
    if splits == 0:
        print("Invalid split count")
    elif splits == 1:
        return 0
    elif splits == 2:
        if np.isnan(value) or value < min_value + (max_value - min_value) / 2:
            return 0
        else:
            return 1
    else:
        step = (max_value - min_value) / (splits - 2)
        
        if np.isnan(value) or value < min_value:
            return 0
        elif (value >= max_value):
            return splits - 1
        else:
            return int((value - min_value) / step + 1)


# In[ ]:


# A helper method for the HMM that converts three separate split ids found with get_split_id() method, into one
# classes_count param represents the total amount of splits specified in the configuration
def get_multiplication_class_id(class_id_1, class_id_2, class_id_3, classes_count):
    return classes_count*classes_count*class_id_1 + classes_count*class_id_2 + class_id_3


# In[25]:


##############################
## METHOD IMPLEMENTATIONS ####
##############################


# In[26]:


# K-NN implementation
def use_k_nearest_neighbour(train_X, train_y, test_X, test_y, k):
    model = neighbors.KNeighborsClassifier(n_neighbors=k).fit(train_X, train_y)
    pred_y = model.predict(test_X)
    return pred_y


# In[27]:


# We also first experimented with the svm model. But later left out of the thesis.
def use_svm(train_X, train_y, test_X, test_y):
    model = svm.SVC().fit(train_X, train_y)
    pred_y = model.predict(test_X)    
    return pred_y


# In[28]:


# Method used for the HMM approach that was finally decided to be excluded from the thesis.
# We still include it in the source code for informational reasons.
# This apprach used Gaussian distribution for the RSSI value distribution. 
def use_viterbi_means(train_X, train_y, test_X, test_y, conf, plot_matrix=True):    
    model = hmm.GaussianHMM(n_components=len(ROOMS), covariance_type="full")
    model.startprob_ = np.array([1/len(ROOMS)]*len(ROOMS))
    model.transmat_ = get_hardcoded_transition_matrix(conf["initial-stay-prob"], conf["omega"]) if conf["trans-matrix-hardcoded"] else get_non_hardcoded_transition_matrix(train_y,conf["omega"])
    
    means, stds = get_means_and_std(train_X, train_y)
    
    model.means_ = means
    # This specifies the standard deviation for the Gaussian distribution.
    model.covars_ = 100 * np.tile(np.identity(3), (8, 1, 1))
    covars = np.zeros([len(ROOMS), 3, 3])
    
    for i, room in enumerate(ROOMS):
        mtx = covars[i]
        std = stds[i]
        for j in range(len(std)):
            mtx[j][j] = conf['standard-deviation']**2 if conf['standard-deviation'] != "custom" else std[j]**2 + 1
    
    # Instead covars matrix is used instead of the regular observation matrix.
    model.covars_ = covars
    total = 0
    correct = 0
    
    prob, states = model.decode(np.array(test_X))
    predicted_y = get_state_names(states)

    return predicted_y


# In[57]:


# Method used to predict the rooms with the HMM and Viterbi.

def use_viterbi_multinomial(train_X, train_y, test_X, test_y, hmm_conf, visualisation_conf):
    splits = hmm_conf["multinomial-splits"]
    min_value = hmm_conf["multinomial-min_value"]
    max_value = hmm_conf["multinomial-max_value"]

    # First for every room and for every sensor the probability distribution is created
    emissions = {}
    for room in ROOMS:
        for sensor in get_sensor_names(train_X):
            emissions[str(room) + "_" + str(sensor)] = get_emissions(train_X, train_y, room, sensor, splits=splits)

    # The created distributions can also be visualised if specified in the configuration
    if visualisation_conf["multinomial-visualize"]:
        visualize_emissions(emissions, get_sensor_names(train_X), hmm_conf)
    
    n_components = len(ROOMS)
    
    # The actual creation of the HMM
    model = hmm.MultinomialHMM(n_components=n_components)
    
    # Specifies the starting probabilties. We used the same starting probability for each room
    model.startprob_ = np.array([1/n_components] * n_components) 
    
    # Specifies the transition probabilities. Based on the conf, we use hardcoded or non-hardcoded transition matrix
    model.transmat_ = get_hardcoded_transition_matrix(hmm_conf["initial-stay-prob"], hmm_conf["omega"]) if hmm_conf["trans-matrix-hardcoded"] else get_non_hardcoded_transition_matrix(train_y, hmm_conf["omega"])
    
    # Specifies the emission probabilities.
    model.emissionprob_ = get_emission_matrix(emissions, get_sensor_names(train_X))
    
    ###############################################################################################################
    ## The below code converts the observations into an id system that can be used with the hmm.MultinomialHMM() ##
    ###############################################################################################################
    
    converted_X = []
    for el in test_X.values:
        class_1 = get_split_id(el[0], splits, min_value, max_value)
        class_2 = get_split_id(el[1], splits, min_value, max_value)
        class_3 = get_split_id(el[2], splits, min_value, max_value)
        class_id = get_multiplication_class_id(class_1, class_2, class_3, splits)
        converted_X.append(class_id)
    
    converted_X = np.atleast_2d(converted_X).T
    _, predictions = model.decode(converted_X)

    # As the predictions are just ids, we also need to convert them into the room labels
    predicted_rooms = get_state_names(predictions)
    
    return predicted_rooms


# In[64]:


# This method is the main linkage between machine learning methods and the input
# It splits the data into RSSI data and labels and manages all the configuration details

def train_and_get_accurracy(train_data, test_data, method, hmm_conf, general_conf, visualisation_conf):
    # Resets indexes in input data because of the pandas specifics
    train_data = train_data.copy().reset_index(drop=True)
    test_data = test_data.copy().reset_index(drop=True)
    
    # Extracts some of the configuartion details into variables
    shuffle = general_conf["shuffle"]
    train_usage = general_conf["train_usage"]
    test_usage = general_conf["test_usage"]
    k = general_conf["k"]
    
    # Tests if any input is unlogical or wrong
    if has_errors(train_data, test_data, train_usage, test_usage, shuffle, method, general_conf):
        return
    
    # Based on the configuration converts data into train_X, train_y, test_X and test_y. X means RSSI data and y are the labels
    train, test = get_train_and_test_datasplits(train_data, test_data, train_usage, test_usage, shuffle, general_conf) 
    train_X, train_y, test_X, test_y = split_X_y(train, test)
    
    # Chooses the specified machine learning method
    if method == 'knn':
        pred_y = use_k_nearest_neighbour(train_X, train_y, test_X, test_y, k)
    elif method == 'svm':
        pred_y = use_svm(train_X, train_y, test_X, test_y)
    elif method == "hmm":
        if hmm_conf['type'] == "means":
            pred_y = use_viterbi_means(train_X, train_y, test_X, test_y, hmm_conf)
        elif hmm_conf['type'] == "multinomial":
            pred_y = use_viterbi_multinomial(train_X, train_y, test_X, test_y, hmm_conf, visualisation_conf)
            
    # Based on the visualisation conf, run the visualisation methods
    if visualisation_conf['show-path-visualisation']:
        visualize_predictions(test_y, pred_y)
    if visualisation_conf['show-confusion-matrix']:
        plot_confusion_matrix(test_y, pred_y, title=method if method!="hmm" else hmm_conf["algorithm"])
    if visualisation_conf['show-room-level-acc']:
        plot_room_level_acc(test_y, pred_y)
    return accuracy_score(pred_y, test_y)


# In[34]:


non_visualize_conf = {
    "show-path-visualisation": False,
    "show-confusion-matrix": False,
    "show-room-level-acc": False,
    "test": False
}


# In[35]:


dataset_lounge = load_recordings_by_nrs(["01","02","03","04","05"])
dataset_study = load_recordings_by_nrs(["07","08","09","10"])


# In[44]:


#########################
## MAIN STARTUP CELL ####
#########################


# In[86]:


###########################################
## General data processing configuration ##
###########################################
general_conf = {
    "nan-fill": None, # Value to repalce the empty values. None means that no replacing is done
    "avg": None, # Specifies the number of frames over which the RSSI data is averaged. None that this is not done
    "k": 200, # Used only when train_and_get_accurracy is called with method="knn"
    "shuffle": False, # Suffles both, the training and testing data
    "train_usage": 1, # Value from 0.0-1.0. Specifies the amount of recording being used in percentage
    "test_usage": 1   # Value from 0.0-1.0. Specifies the amount of recording being used in percentage
}


####################################################################################################
## HMM specific conf - this is used only when train_and_get_accurracy is called with method="hmm" ##
####################################################################################################
hmm_conf = {
    # Options: ['Viterbi']
    "algorithm": "viterbi", 
    
    # Options: ['means', 'multinomial']. 
    # We used 'multinomial' in this thesis. Means uses the guassian distributions for the emission matrix
    "type": "multinomial",  
    
    # Options: ['custom', any int value]
    # Used only when above is set to 'means'. Sets the std of the distribution
    "standard-deviation": "custom", 
    
    # Options: [True, False]
    # Used only when "type" = "multinomial"
    # Specifies if a hardcoded or non-hardcoded transition matrix will be used. 
    "trans-matrix-hardcoded": False,
    
    # Used only when hardcoded transition matrix is used
    "initial-stay-prob": 0.9999,
    
    # Omega in our proposed approach. 
    # Omega=0 -> does not change anything
    # Omega>0 -> Reduces the room switchin probability by 10**omega
    "omega": 0,
    
    # Number of splits the sensor values will be divided into
    "multinomial-splits": 7,
    
    # The range where most of the RSSI values fall into. This changes the splitting process
    "multinomial-min_value": -110,
    "multinomial-max_value": -60 
}


######################################################################################
## Visualisation conf - used for most of the data visualisations used in the thesis ##
######################################################################################
visualisation_conf = {
    "show-path-visualisation": False,
    "show-confusion-matrix": True,
    "show-room-level-acc": False,
    "multinomial-visualize": False
}



# Calling the train_and_get_accurracy method will train and test a method one time.
# It will use the parameters and settings specified above and in the actual method call.
acc = train_and_get_accurracy(train_data   = dataset_lounge[0], # Sets the training recording to be with id=0
                        test_data          = dataset_lounge[1], # Sets the testing recording to be with id=1
                        method             = "knn", # Possible options: hmm, knn, svm
                        hmm_conf           = hmm_conf, # Used only, when method is hmm
                        general_conf       = general_conf,
                        visualisation_conf = visualisation_conf
                       )

print("Accurracy:", acc)


# In[56]:


########################################################################
## VALIDATION METHODS AND MULTICORE NESTED CROSS VALIDATION METHODS ####
########################################################################


# In[ ]:


# This is a helper method for the validation process described in the thesis.
# This method extracts the 10-minute windows (or any other length specified by len_of_window param)
# It will also take the increment_len as a paramter that specifies the amount to move the window forward in time
def get_splits(data, len_of_window, increment_len, include_reversed=True):
    splits = []
    splits_reversed = []
    start_time = 0

    for time in range(start_time, len(data)-len_of_window, increment_len):
        split = data[time:time+len_of_window].copy()
        splits.append(split)
        if include_reversed:
            splits_reversed.append(split[::-1])
            
    # Leftover is also added to the splits
    split = data[time+len_of_window:]
    splits.append(split)
    splits_reversed.append(split[::-1])
    return splits, splits_reversed


# In[94]:


# The method evaluates the final accuracy of the specified machine learning method
# Besides the configuration, multiple training recordings are taken as the parameter. Also one recording for testing. 
def final_evaluation(train_sets, test, method, general_conf, hmm_conf):
    len_of_one_second = 20
    len_of_test_window = len_of_one_second * 60 * 10 # 10 minutes
    increment_len = len_of_one_second * 60 * 1 # 1 minute  
    
    # Non-reversed and reversed accuracies are measured separately
    accs = []
    accs_rev = []
    
    # The test data is converted into 10-minute windows
    test_splits, test_splits_rev = get_splits(test, len_of_test_window, increment_len, True)
    
    # The model gets trained with each of the input training recordings
    for train in train_sets:
        for i in range(len(test_splits)):
            accs.append(train_and_get_accurracy(train_data   = train,
                        test_data          = test_splits[i],
                        cross_val_data     = None,
                        method             = method,
                        hmm_conf           = hmm_conf,
                        general_conf       = general_conf,
                        visualisation_conf = non_visualize_conf
                       ))
            if method != "knn":
                accs_rev.append(train_and_get_accurracy(train_data   = train,
                        test_data          = test_splits_rev[i],
                        cross_val_data     = None,
                        method             = method,
                        hmm_conf           = hmm_conf,
                        general_conf       = general_conf,
                        visualisation_conf = non_visualize_conf
                       ))
    # Finally an average accuracy of both non-reversed and reversed testing data is returned.
    return np.average(accs), np.average(accs_rev)


# In[96]:


# The implementation of the inner cross-validation used in the thesis
# In the inner cross-validation one training- and multiple testing recordings must be provided. 
# param reverse_order specifies if the specific inner cross-validation uses the reversed testing data or non-reversed
def inner_cross_validation(method, train, testing_sets, reverse_order, hmm_conf, general_conf, visualisation_conf):    
    len_of_one_second = 20
    len_of_test_window = len_of_one_second * 60 * 10 # 10 minutes
    increment_len = len_of_one_second * 60 * 1 # 1 minute

    accs = []
    
    # Each testing recording gets converted into 10-minute splits and every split is tested with the trained model
    for i, test in enumerate(testing_sets):
        
        splits = get_splits(test, len_of_test_window, increment_len, True)
        if reverse_order:
            splits = splits[1]
        else:
            splits = splits[0]
    
        
        for test in splits:
            acc = train_and_get_accurracy(train_data   = train,
                            test_data          = test,
                            cross_val_data     = None,
                            method             = method,
                            hmm_conf           = hmm_conf, # Used only, when method is hmm
                            general_conf       = general_conf,
                            visualisation_conf = visualisation_conf)    
            accs.append(acc)
            
    # Finally an average accuracy is returned
    return np.average(accs)


# In[ ]:


##############################################################################################################
## In the following three cells are the methods to perform multithread cross-validation for every ML method ##
##############################################################################################################


# In[98]:


# Methods to perform the k-NN nested cross-validation with multiple threads

def knn_impl_helper(params):
    k_value = params[0]
    avg = params[1]
    train = params[2]
    test = params[3]
    info = params[4]
 
    general_conf = {
        "nan-fill": -110,
        "avg": avg,
        "k": k_value,
        "shuffle": False,
        "train_usage": 1,
        "test_usage": 1
    }
    acc = train_and_get_accurracy(train_data = train, test_data = test, cross_val_data = None,method = "knn", hmm_conf = hmm_conf, general_conf = general_conf, visualisation_conf = non_visualize_conf)                   
    return acc, info
                                  
# cross_datas - recordings on which to perform the cross-validation
# filename - output filename
# ks - a list of all the values of k to test
# avgs - a list of all the param avg values to test
# densities - a list of data densities to test. Finally not used in the thesis
def knn_multithread_starter(cross_datas, filename, ks, avgs, densities=[1]):
    # params_list specifies the conf of the task to perform.
    params_list = []
    datas = cross_datas

    for density in densities:
        for avg in avgs:
            for k in ks:                  
                for i, train in enumerate(datas):
                    for j, test in enumerate(datas):
                        if (i==j):
                            continue
                        params_list.append([k, avg, train[::density], test[::density], ["knn",avg, k, False, "::"+str(density), "train_"+str(i+1), "test_"+str(j+1)]])

    # A multithread strarter to speed up the training process. However, running these methods take hours.
    pool = Pool()
    knn_results = pool.map(knn_impl_helper, params_list)
    pool.close()
    pool.join()
    with open('results/'+ filename + '.pkl', 'wb') as f:
        pickle.dump(knn_results, f, pickle.HIGHEST_PROTOCOL)
    return knn_results


# In[104]:


# Methods to perform the HMM nested cross-validation with multiple threads

def knn_impl_helper(params):
    split = params[0]
    omega = params[1]
    train = params[2]
    test_sets = params[3]
    density = params[4]
    hardcoded = params[5]
    reverse_order = params[6]
    info = params[7]
    
    hmm_conf = {
        "algorithm": "viterbi",
        "type": "multinomial",
        "standard-deviation": "custom",
        "trans-matrix-hardcoded": hardcoded,
        "initial-stay-prob": 0.9999,
        "omega": omega, 
        "trans-matrix-hardcoded-switch-prob": prob,

        "multinomial-splits": split,
        "multinomial-min_value": -110,
        "multinomial-max_value": -60,
        "multinomial-visualize": False
    }

    general_conf = {
        "nan-fill": None,
        "avg": None,
        "k": 600,
        "shuffle": False,
        "train_usage": 1,
        "test_usage": 1
    }

    acc = inner_cross_validation("hmm", train[::density], test_sets, reverse_order, hmm_conf, general_conf, non_visualize_conf)   
    return acc, info

# cross_datas - recordings on which to perform the cross-validation
# filename - output filename
# densities - a list of data densities to test. Finally not used in the thesis
# hardcoded - Use hardcoded transition matrix?
# omegas - a list of all the values of omega to test
# splits - a list of all the split counts to test

def hmm_hard_multithread_starter(cross_dataset, filename, densities, hardcoded, omegas, splits):
    # params_list specifies the conf of the task to perform.
    params_list = []
    datas = cross_dataset

    for density in densities:
        for split in splits:
            for omega in omegas:
                for i, train in enumerate(datas):
                    datas_copy = datas.copy()
                    datas_copy.pop(i)
                    test_sets = datas_copy
                    params_list.append([split, prob, train, test_sets, density, hardcoded, False, 
                                        ["hmm-mult-hard-non_rev",split, omega, "::"+str(density), "train_" + str(i+1)]])
                    params_list.append([split, prob, train, test_sets, density, hardcoded, True, 
                                        ["hmm-mult-hard-rev",split, omega, "::"+str(density), "train_" + str(i+1)]])

    # A multithread strarter to speed up the training process. However, running these methods take hours.
    pool = Pool()
    hmm_results = pool.map(knn_impl_helper, params_list)
    with open('results/'+ filename + '.pkl', 'wb') as f:
        pickle.dump(hmm_results, f, pickle.HIGHEST_PROTOCOL)
    pool.close()
    pool.join()
    return hmm_results
    

